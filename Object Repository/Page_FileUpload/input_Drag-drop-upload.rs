<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Drag-drop-upload</name>
   <tag></tag>
   <elementGuidId>d375c3a8-71e5-4eec-b41a-6414b0964334</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='drag-drop-upload']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#drag-drop-upload</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>a3d3e6a5-b685-4b0c-b583-e9a3b7fd3204</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dz-success-mark dz-clickable</value>
      <webElementGuid>446e6b37-c842-4cb0-96e6-8a4c5c69fd68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>drag-drop-upload</value>
      <webElementGuid>937c19fe-4675-4982-b8d6-56ceb136b499</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;drag-drop-upload&quot;)</value>
      <webElementGuid>5b57f44c-a323-4f4d-a7ce-75532aa04016</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='drag-drop-upload']</value>
      <webElementGuid>05f12bc4-9feb-469e-aee6-39f87b8921a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div[2]</value>
      <webElementGuid>e861d8cc-fcca-4ebe-9540-10835781de07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='File Uploader'])[1]/following::div[1]</value>
      <webElementGuid>a9062830-0d0f-4787-98cc-7d2047b83652</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='✔'])[1]/preceding::div[3]</value>
      <webElementGuid>e70ffb93-0194-483a-8652-4c97d454c678</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]</value>
      <webElementGuid>e2a73f51-efc8-4f92-bed4-bf2ae453c910</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'drag-drop-upload']</value>
      <webElementGuid>fc85bd33-76e3-445e-b14b-e639e3c3d973</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
