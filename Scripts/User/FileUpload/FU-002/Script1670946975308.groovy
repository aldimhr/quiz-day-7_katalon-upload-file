import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser(GlobalVariable.G_BaseURL)

WebUI.setViewPortSize(1366, 699)

WebUI.waitForElementPresent(findTestObject('Page_Homepage/txt_Welcome to the-internet'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Homepage/txt_Welcome to the-internet'), GlobalVariable.G_Timeout)

WebUI.click(findTestObject('Page_Homepage/btn_File Upload'))

WebUI.waitForElementPresent(findTestObject('Page_FileUpload/txt_File Uploader'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_FileUpload/txt_File Uploader'), GlobalVariable.G_Timeout)

userDir = System.getProperty('user.dir')

for(def rowNum = 1; rowNum <= findTestData('Data Upload').getRowNumbers(); rowNum++) {
	currentData = findTestData('Data Upload').getValue(1, rowNum)
	
	filePath = ((userDir + '/Data Files/') + currentData)
	
	WebUI.uploadFile(findTestObject('Page_FileUpload/input_File Upload'), filePath)
}

WebUI.closeBrowser()

